package sqliteMgrJ;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.net.URL;

public class Main extends Application {

    private static Stage _primaryStage;

    public static Stage getPrimaryStage(){
        return _primaryStage;
    }

    @Override
    public void start(Stage primaryStage) throws Exception{

        _primaryStage = primaryStage;

        URL landingUrl = getClass().getResource("/sqliteMgrJ/view/MainPane.fxml");

        if (landingUrl == null){
            throw new NullPointerException("landingUrl is null.");
        }

        Parent root = FXMLLoader.load(landingUrl);
        primaryStage.setTitle("Sqlite Manager");
        Scene scene = new Scene(root, 1280, 850);
        primaryStage.setScene(scene);

        try{
            ObservableList<String> styleSheets = scene.getStylesheets();
            styleSheets.add("/sqliteMgrJ/view/SqlSyntaxStyle.css");
            System.out.println("Css added");
        }
        catch (Exception ex){
            ex.printStackTrace();
        }

        primaryStage.getIcons().add(new Image("/sqliteMgrJ/image/sqlite.png"));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
