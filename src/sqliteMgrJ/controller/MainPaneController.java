package sqliteMgrJ.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.image.ImageView;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.TransferMode;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import sqliteMgrJ.helper.EffectsHelper;
import sqliteMgrJ.helper.ImageHelper;
import sqliteMgrJ.helper.MessageBoxHelper;
import sqliteMgrJ.helper.SqlHelper;
import sqliteMgrJ.model.*;


import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class MainPaneController {

    @FXML
    private TreeView<NodeInfo> treeView;

    @FXML
    private TextField tfSearch;


    @FXML
    private TextArea txtDescription;

    @FXML
    private TabPane tabPaneSqlQueries;

    private int tabSeq;

    @FXML
    private ContextMenu ctxMenuInTree;



    private List<TreeItem<NodeInfo>> matchingItems = new ArrayList<>();

    // the initialize method is automatically invoked by the FXMLLoader - it's magic
    public void initialize() {
        setupTree();

        setupSearch();

        this.treeView.setOnContextMenuRequested(event -> {
            ctxMenuInTree.getItems().clear();

            TreeItem<NodeInfo> treeItem = this.getSelectedNode();
            if (treeItem != null){
                NodeInfo nodeInfo = treeItem.getValue();
                if (nodeInfo instanceof TableNodeInfo) {
                    MenuItem mi = new MenuItem("Show first 100 rows");
                    mi.setOnAction(e -> {
                        showFirstBatchRows();
                    });
                    ctxMenuInTree.getItems().add(mi);
                }
            }

            event.consume();
        });
    }


    private void setupSearch() {
        this.tfSearch.setOnKeyReleased(event -> {
            matchingItems.clear();
            searchByKeyword(tfSearch.getText().toLowerCase(), treeView.getRoot(), EffectsHelper.SearchMatched);
        });
    }

    private void searchByKeyword(String text, TreeItem<NodeInfo> treeItem, Effect effect) {

        NodeInfo data = treeItem.getValue();

        if (data != null) {
            if (text != null && (text.length() > 0) && data.getDisplayName() != null && data.getDisplayName().toLowerCase().contains(text)){
                Node node = treeItem.getGraphic();
                node.setEffect(effect);
                matchingItems.add(treeItem);

            }else{
                treeItem.getGraphic().setEffect(null);
            }
        }

        for(TreeItem<NodeInfo> subNode : treeItem.getChildren()){
            searchByKeyword(text, subNode, effect);
        }
    }

    public void setupTree(){
        try {
            final TreeItem<NodeInfo> root = new TreeItem(new NodeInfo(), new ImageView(ImageHelper.GetPath("icon_SqlDataSource.png")));
            root.setExpanded(true);
            treeView.setRoot(root);

            // hide the root node
            treeView.setShowRoot(false);


            treeView.setOnDragDropped( event -> {
                Dragboard dragboard = event.getDragboard();
                if (dragboard.hasFiles()){
                    for( File file : dragboard.getFiles()){
                        addDbFile(file);
                    }
                }
                event.consume();
            });

            treeView.setOnDragOver(event -> {
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                event.consume();
            });

            treeView.setOnKeyPressed(event -> {
                if (event.getCode() == KeyCode.F2) {
                    TreeItem<NodeInfo> item = treeView.getSelectionModel().getSelectedItem();
                    NodeInfo data = item.getValue();
                    System.out.println("Definition: " + data.getDisplayName());
                }
            });

            treeView.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2) {
                }

                //this.ctxMenuInTree.setsh
            });

            treeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {

                TreeItem<NodeInfo> ti = getSelectedNode();

                if (ti != null){
                    NodeInfo nodeInfo = ti.getValue();
                    txtDescription.setText(nodeInfo.getDescription());
                }
            });

        } catch (Exception ex) {
            MessageBoxHelper.ShowException(ex);
        }


    }


    public void removeDatabase(Event event) {

        TreeItem<NodeInfo> ti = getCurrentDatabaseTreeItem();

        if (ti != null){
            if (ButtonType.OK != MessageBoxHelper.Confirm("Confirm", "Removing Database", "Are you sure you want to remove the selected database?")){
                return;
            }

            ti.getParent().getChildren().remove(ti);
        }
    }


    public void chooseFile(Event event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        File file = fileChooser.showOpenDialog(this.tfSearch.getScene().getWindow());

        if (file != null) {
            this.addDbFile(file);
        }
    }

    public SqlQueryPaneController addQueryPane(Event event) {
        try {
            TreeItem<NodeInfo> dbTreeItem = this.getCurrentDatabaseTreeItem();
            if (dbTreeItem != null){
                URL landingUrl = getClass().getResource("/sqliteMgrJ/view/SqlQueryPane.fxml");

                if (landingUrl == null){
                    throw new NullPointerException("landingUrl is null.");
                }

                FXMLLoader fxmlLoader = new FXMLLoader();
                Parent root = fxmlLoader.load(landingUrl.openStream());

                SqlQueryPaneController sqlQueryPaneController = fxmlLoader.getController();
                DatabaseNodeInfo dbNodeInfo = (DatabaseNodeInfo)dbTreeItem.getValue();
                sqlQueryPaneController.setDatabaseNodeInfo(dbNodeInfo);

                Tab tab = new Tab(dbNodeInfo.getName() + "|Query " + (++this.tabSeq));
                tab.setContent(root);


                this.tabPaneSqlQueries.getTabs().add(tab);
                this.tabPaneSqlQueries.getSelectionModel().select(tab);

                return sqlQueryPaneController;
            }
        } catch (IOException ex) {
            MessageBoxHelper.ShowException(ex);
        }

        return  null;
    }

    private void addDbFile(File file) {
        DatabaseNodeInfo databaseNode = new DatabaseNodeInfo(file.getName(), file.getAbsolutePath());
        TreeItem<NodeInfo> ti = this.createDatabaseNode(databaseNode);

        // get the root item and add the new tree item to it.
        TreeItem root = treeView.getRoot();
        root.getChildren().add(ti);

        // make the new database node selected.
        treeView.getSelectionModel().select(ti);
    }

    private TreeItem<NodeInfo> createDatabaseNode(DatabaseNodeInfo databaseNode){
        ImageView iv = new ImageView(ImageHelper.GetPath("database.png"));
        iv.setFitHeight(16);
        iv.setFitWidth(16);

        TreeItem ti = new TreeItem(databaseNode, iv);
        ti.setExpanded(true);
        this.createDatabaseNode(ti);

        return ti;
    }

    private void createDatabaseNode(TreeItem<NodeInfo> dbTreeItem) {
        DatabaseNodeInfo dbNodeData = (DatabaseNodeInfo) dbTreeItem.getValue();

        dbNodeData.getTables().clear();

        String connStr = "jdbc:sqlite:" + dbNodeData.getFilePath();
        dbNodeData.setConnectionString(connStr);

        //Connection connection = null;
        try(Connection connection = DriverManager.getConnection(connStr))
        {
            SqlHelper helper = new SqlHelper(connection);
            new DatabaseMetaService(helper).reloadMetadata(dbNodeData);

            // add tables aggregation node
            TreeItem<NodeInfo> tablesItem = new TreeItem(new NodeInfo("Tables"), ImageHelper.FromFile("Folder_16x16.png", 16));
            dbTreeItem.getChildren().add(tablesItem);

            // add views aggregation node
            TreeItem<NodeInfo> viewsItem = new TreeItem(new NodeInfo("Views"), ImageHelper.FromFile("Folder_16x16.png", 16));
            dbTreeItem.getChildren().add(viewsItem);

            dbNodeData.getTables().forEach(tableNodeInfo -> {
                TreeItem<NodeInfo> tableTreeItem;

                if (tableNodeInfo.isTable()) {
                    // table
                    tableTreeItem = new TreeItem(tableNodeInfo, ImageHelper.FromFile("Table_32.png", 16));
                    tablesItem.getChildren().add(tableTreeItem);

                    this.setupColumns(tableNodeInfo, tableTreeItem);
                    this.setupIndexes(tableNodeInfo, tableTreeItem);
                    this.setupTriggers(tableNodeInfo, tableTreeItem);
                } else {
                    // view
                    tableTreeItem = new TreeItem(tableNodeInfo, ImageHelper.FromFile("dataView.png"));
                    viewsItem.getChildren().addAll(tableTreeItem);
                    this.setupColumns(tableNodeInfo, tableTreeItem);
                }
            });
        }
        catch(Exception ex)
        {
            MessageBoxHelper.ShowException(ex, "Error", "Failed to open database");
        }
    }

    private void setupTriggers(TableNodeInfo tableNodeInfo, TreeItem<NodeInfo> tableTreeItem) {
        TreeItem<NodeInfo> triggersItem = new TreeItem(new NodeInfo("Triggers"), ImageHelper.FromFile("Folder_16x16.png", 16));
        tableTreeItem.getChildren().add(triggersItem);

        for(TriggerNodeInfo triggerNodeInfo : tableNodeInfo.getTriggers()){
            triggersItem.getChildren().add(new TreeItem(triggerNodeInfo, ImageHelper.FromFile("trigger.png", 16)));
        }
    }

    private void setupIndexes(TableNodeInfo tableNodeInfo, TreeItem<NodeInfo> tableTreeItem) {
        TreeItem<NodeInfo> idxesItem = new TreeItem(new NodeInfo("Indexes"), ImageHelper.FromFile("Folder_16x16.png", 16));
        tableTreeItem.getChildren().add(idxesItem);

        for(IndexNodeInfo index : tableNodeInfo.getIndexes()){
            idxesItem.getChildren().add(new TreeItem(index, ImageHelper.FromFile("Index.png", 16)));
        }
    }

    private void setupColumns(TableNodeInfo tableNodeInfo, TreeItem<NodeInfo> tableTreeItem) {
        TreeItem<NodeInfo> columnsNode = new TreeItem(new NodeInfo("Columns"), ImageHelper.FromFile("Folder_16x16.png", 16));
        tableTreeItem.getChildren().add(columnsNode);
        tableNodeInfo.getColumns().forEach(columnMetadata -> {
            String imgUrl = "Column.png";
            if (columnMetadata.getPk() == 1) {
                imgUrl = "PrimaryKeyHS.png";
            }
            ImageView ivColumn = ImageHelper.FromFile(imgUrl, 16);

            TreeItem colNode = new TreeItem(new NodeInfo(columnMetadata.getName() + ", " + columnMetadata.getType()), ivColumn);
            columnsNode.getChildren().add(colNode);
        });
    }



    private TreeItem<NodeInfo> getSelectedNode(){
        return this.treeView.getSelectionModel().getSelectedItem();
    }

    private TreeItem<NodeInfo> getCurrentDatabaseTreeItem(){
        TreeItem<NodeInfo> iter = this.getSelectedNode();

        while( iter != null && !(iter.getValue() instanceof DatabaseNodeInfo)) {
            iter = iter.getParent();
        }

        return iter;
    }

    public void showFirstBatchRows() {
        SqlQueryPaneController ctrl = this.addQueryPane(null);

        TreeItem<NodeInfo> treeItem = this.getSelectedNode();
        NodeInfo nodeInfo = treeItem.getValue();
        if (nodeInfo instanceof TableNodeInfo) {
            TableNodeInfo tableNodeInfo = (TableNodeInfo)nodeInfo;
            ctrl.runSql("SELECT * FROM " + tableNodeInfo.getName() + " LIMIT 100");
        }
    }

    public void refreshDatabase(Event event) {
        TreeItem ti = this.getCurrentDatabaseTreeItem();

        if (ti != null){
            ti.getChildren().clear();
            this.createDatabaseNode(ti);
            treeView.getSelectionModel().select(ti);
        }
    }


    public void goPrevious(Event event) {
        goSearch(-1);
    }

    public void goNext(Event event) {
        goSearch(1);
    }

    private void goSearch(int step){

        if(matchingItems.size() == 0) {
            return;
        }

        TreeItem<?> treeItem = this.treeView.getSelectionModel().getSelectedItem();

        TreeItem<NodeInfo> treeItemToSelect;
        if (treeItem == null){
            treeItemToSelect = matchingItems.get(0);
        }
        else {
            int index = matchingItems.indexOf(treeItem);

            if (index == -1){
                index = 0;
            }
            else {
                index += step;

                if (index == -1) {
                    index = matchingItems.size() - 1;
                }
                else if (index == matchingItems.size()){
                    index = 0;
                }
            }

            treeItemToSelect = matchingItems.get(index);
        }

        if (treeItemToSelect != null){
            treeView.getSelectionModel().select(treeItemToSelect);
            // treeView.scrollTo(treeView.getSelectionModel().getSelectedIndex());
        }
    }
}
