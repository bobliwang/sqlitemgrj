package sqliteMgrJ.controller;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Font;
import javafx.stage.PopupWindow;
import javafx.util.Callback;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.PlainTextChange;
import org.reactfx.EventStream;
import sqliteMgrJ.helper.SqlHelper;
import sqliteMgrJ.helper.SqlStyleHelper;
import sqliteMgrJ.helper.StringHelper;
import sqliteMgrJ.model.DatabaseNodeInfo;
import sqliteMgrJ.model.actions.IntelliSenseKeyAction;
import sqliteMgrJ.model.actions.KeyAction;
import sqliteMgrJ.model.shortcuts.SqlShortCut;


import java.sql.*;
import java.time.Duration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * Created by Airloom-Lee-MBP on 11/11/2014.
 */
public class SqlQueryPaneController {

    @FXML
    private Button btnRunSql;

    @FXML
    private TableView tableViewResults;

    @FXML
    private CodeArea txtSql;

    @FXML
    private TextArea txtLogs;

    @FXML
    private Label lblStatus;

    @FXML
    private Tab tabLogs;

    @FXML
    private TabPane tabPane;

    private DatabaseNodeInfo databaseNodeInfo;

    private IntelliSenseKeyAction intelliSenseKeyAction;

    private Alert alert;


    // the initialize method is automatically invoked by the FXMLLoader - it's magic
    public void initialize() {

        txtSql.setFont(new Font("Verdana", 14));
        this.txtLogs.setCursor(Cursor.TEXT);
        tableViewResults.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        tableViewResults.setOnKeyPressed(event -> {
            if ((event.isControlDown() || event.isShortcutDown()) && event.getCode() == KeyCode.C) {
                ObservableList<ObservableList<Object>> selectedItems = tableViewResults.getSelectionModel().getSelectedItems();
                StringBuilder sb = new StringBuilder();
                ObservableList<TableColumn> columns = tableViewResults.getColumns();
                sb.append(String.join(",", columns.stream().map(x -> "\"" + x.getText() + "\"").collect(Collectors.toList())) + "\r\n");

                if (selectedItems != null){

                    for(ObservableList<Object> row : selectedItems){
                        String line = String.join(",", row.stream().map(x -> "\"" + (x != null ? x.toString().replace("\"", "\"\"") : "") + "\"").collect(Collectors.toList()));
                        sb.append(line + "\r\n");
                    }
                }

                final Clipboard clipboard = Clipboard.getSystemClipboard();
                final ClipboardContent content = new ClipboardContent();
                content.putString(sb.toString());
                clipboard.setContent(content);
            }
        });

        this.txtSql.setOnKeyPressed(event -> {

            if (event.isControlDown() && event.getCode() == intelliSenseKeyAction.getKeyCode() ) {
                intelliSenseKeyAction.perform(event, this.txtSql);
            }
            else {

                if (event.getCode() == KeyCode.ESCAPE){
                    PopupWindow pw = this.txtSql.getPopupWindow();
                    if(pw != null && pw.isShowing()){
                        pw.hide();
                    }
                }
                else if (event.getCode() == KeyCode.F5 || event.getCode() == KeyCode.F9) {
                    this.runSql();
                }
                else {
                    for(KeyAction keyAction : KeyAction.PredefinedKeyActions){
                        if (keyAction.getKeyCode() == event.getCode()){

                            keyAction.perform(event, this.txtSql);

                            if (event.isConsumed()){
                                String currentWord = StringHelper.getCurrentWord(txtSql).trim();
                                if (currentWord.startsWith("{") && currentWord.endsWith("}")){
                                    intelliSenseKeyAction.perform(event, txtSql);
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    private void startSyntaxHighlightingTask() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        this.txtSql.setParagraphGraphicFactory(LineNumberFactory.get(this.txtSql));
        EventStream<PlainTextChange> textChanges = this.txtSql.plainTextChanges();

        SqlStyleHelper helper = new SqlStyleHelper(this.txtSql, executor, this.databaseNodeInfo);
        textChanges
                .successionEnds(Duration.ofMillis(500))
                .supplyTask(helper::computeHighlightingAsync)
                .awaitLatest(textChanges)
                .subscribe(helper::applyHighlighting);
    }

    public void runSql(String sql) {

        this.txtSql.appendText(sql);

        this.runSql();
    }

    private void runSql() {
        String sql = getRunnableSql();

        if (sql == null || sql.isEmpty()){
            return;
        }

        try(Connection connection = DriverManager.getConnection(databaseNodeInfo.getConnectionString())) {
            tableViewResults.getColumns().clear();
            tableViewResults.getItems().clear();;

            SqlHelper sqlHelper = new SqlHelper(connection);
            if (sql.toUpperCase().startsWith("SELECT") || sql.toUpperCase().startsWith("PRAGMA")){
                this.runForResultSet(sqlHelper, sql);
            }
            else {
                this.runForUpdate(sqlHelper, sql);
            }

        }
        catch(Exception ex) {
            StringBuilder sb = getSqlErrorBuilder(sql, ex);

            this.txtLogs.appendText(sb.toString());

            this.tabPane.getSelectionModel().select(this.tabLogs);
        }
    }

    private StringBuilder getSqlErrorBuilder(String sql, Exception ex) {
        StringBuilder sb = new StringBuilder();
        sb.append("Failed to run:" + sql + "\r\n")
                .append("Error Message:" + ex.getMessage() + "\r\n")
                .append("StackTrace:" + StringHelper.getStackTrace(ex));
        return sb;
    }

    private String getRunnableSql() {
        String sql = this.txtSql.getSelectedText();
        if (sql == null || sql.isEmpty()){
            sql = this.txtSql.getText();
        }
        sql = sql.trim();
        return sql;
    }

    private void runForResultSet(SqlHelper sqlHelper, String sql) throws SQLException {
        long startMs = System.currentTimeMillis();
        long total = 0;
        txtLogs.appendText("---------------------------------------\r\n");
        txtLogs.appendText("Executing " + sql + "\r\n");
        try(ResultSet rs = sqlHelper.query(sql)){
            long endMs = System.currentTimeMillis();
            total += (endMs - startMs);
            txtLogs.appendText((endMs - startMs) + "ms running query." + "\r\n");

            startMs = endMs;
            this.displayResultSet(rs, this.tableViewResults);

            endMs = System.currentTimeMillis();
            total += (endMs - startMs);
            txtLogs.appendText((endMs - startMs) + "ms loading data." + "\r\n");

            this.lblStatus.setText(tableViewResults.getItems().size() + " rows returned in " + total + "ms");
        }
    }

    private void runForUpdate(SqlHelper sqlHelper, String sql) throws SQLException {
        long startMs = System.currentTimeMillis();
        long total = 0;
        txtLogs.appendText("---------------------------------------\r\n");
        txtLogs.appendText("Executing " + sql + "\r\n");
        int rowsAffected = sqlHelper.update(sql);
        long endMs = System.currentTimeMillis();
        total += (endMs - startMs);
        txtLogs.appendText((endMs - startMs) + "ms running query." + "\r\n");
        txtLogs.appendText(rowsAffected + " rows affected\r\n");

        TableColumn columnRowsAffected = new TableColumn();
        columnRowsAffected.setText("RowsAffected");
        tableViewResults.getColumns().add(columnRowsAffected);

        TableColumn columnTime = new TableColumn();
        columnTime.setText("TimeMs");
        tableViewResults.getColumns().add(columnTime);

        this.setupCellValueFactory();

        ObservableList<Object> tableRowData = FXCollections.observableArrayList();
        tableRowData.add(rowsAffected);
        tableRowData.add(total);
        tableViewResults.getItems().add(tableRowData);


        this.lblStatus.setText(rowsAffected + " rows affected in " + total + "ms");
    }

    private void setupCellValueFactory(){

        for(int i = 0; i < tableViewResults.getColumns().size(); i++){
            final int j = i;
            TableColumn column = (TableColumn)tableViewResults.getColumns().get(j);
            column.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList<Object>, String>, ObservableValue<String>>() {
                public ObservableValue<String> call(TableColumn.CellDataFeatures<ObservableList<Object>, String> param) {
                    return new SimpleObjectProperty(param.getValue().get(j));
                }
            });
        }
    }

    private void displayResultSet(ResultSet rs, TableView tableViewResults) throws SQLException {
        this.lblStatus.setText("");

        ResultSetMetaData meta = rs.getMetaData();

        // column headers
        for(int i = 0; i < meta.getColumnCount(); i++){
            String colName = meta.getColumnName(i+1);
            TableColumn column = new TableColumn();
            column.setText(colName);
            tableViewResults.getColumns().add(i, column);
        }

        this.setupCellValueFactory();

        while(rs.next()){
            ObservableList<Object> row = FXCollections.observableArrayList();

            for(int i = 0; i < meta.getColumnCount(); i++){
                Object colVal = rs.getObject(i+1);

                if (colVal instanceof byte[]) {
                    colVal = StringHelper.toRowVersion((byte[]) colVal);
                }

                row.add(colVal);
            }

            tableViewResults.getItems().add(row);
        }
    }

    public void btnRunSqlClicked(Event event) {
        this.runSql();
    }


    public void btnTestDlgClicked(Event event) {

        if (alert == null){
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Keyboard Shortcuts");
            alert.setHeaderText("Keyboard Shortcuts");

            alert.getDialogPane().setPrefWidth(600);

            TableView<SqlShortCut> tableView = new TableView<>();
            TableColumn<SqlShortCut, String> shortcutKeysColumn = new TableColumn("ShortCutKey");
            TableColumn<SqlShortCut, String> fullStringColumn = new TableColumn("FullString");

            tableView.getColumns().add(shortcutKeysColumn);
            tableView.getColumns().add(fullStringColumn);

            shortcutKeysColumn.setCellValueFactory(cdf -> new SimpleObjectProperty(cdf.getValue().getShortCutKeys()));
            fullStringColumn.setCellValueFactory(cdf -> new SimpleObjectProperty(cdf.getValue().getFullString()));

            tableView.setItems(FXCollections.observableArrayList(SqlShortCut.getPredefinedShortCuts().stream().sorted((s1, s2) -> s1.getShortCutKeys().compareToIgnoreCase(s2.getShortCutKeys())).collect(Collectors.toList())));
            alert.getDialogPane().setContent(tableView);
        }

        alert.showAndWait();
    }




    public void setDatabaseNodeInfo(DatabaseNodeInfo databaseNodeInfo) {
        this.databaseNodeInfo = databaseNodeInfo;
        this.intelliSenseKeyAction = new IntelliSenseKeyAction(KeyCode.SPACE, databaseNodeInfo != null ? databaseNodeInfo : new DatabaseNodeInfo("N/A"));


        startSyntaxHighlightingTask();

    }
}
