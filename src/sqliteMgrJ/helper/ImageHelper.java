package sqliteMgrJ.helper;

import javafx.scene.image.ImageView;

/**
 * Created by Airloom-Lee-MBP on 13/11/2014.
 */
public class ImageHelper {
    public static String GetPath(String imageFilename){
        return "/sqliteMgrJ/image/" + imageFilename;
    }

    public static ImageView FromFile(String imageFilename) {
        ImageView iv = new ImageView(GetPath(imageFilename));

        return iv;
    }

    public static  ImageView FromFile(String imageFilename, int size){
        ImageView iv = FromFile(imageFilename);
        iv.setFitHeight(size);
        iv.setFitWidth(size);

        return  iv;
    }
}
