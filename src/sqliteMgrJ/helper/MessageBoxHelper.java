package sqliteMgrJ.helper;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 * Created by Airloom-Lee-MBP on 13/11/2014.
 */
public class MessageBoxHelper {
    public static void ShowException(Exception ex, String title, String headerText, boolean printToConsole){

        if (printToConsole) {
            ex.printStackTrace();
        }

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText("Exception Message:" + ex.getMessage());
        alert.showAndWait();
    }

    public static void ShowException(Exception ex, String title, String headerText){
        ShowException(ex, title, headerText, true);
    }

    public static void ShowException(Exception ex, String title){
        ShowException(ex, title, ex.getMessage(), true);
    }

    public static void ShowException(Exception ex){
        ShowException(ex, "Error");
    }

    public static ButtonType Confirm(String title, String headerText, String details) {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(details);
        alert.showAndWait();

        return alert.getResult();
    }

    public static ButtonType Confirm(String title, String headerText) {

        return  Confirm(title, headerText, "");
    }
}
