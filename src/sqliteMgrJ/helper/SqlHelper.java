package sqliteMgrJ.helper;

import sqliteMgrJ.model.ResultSetLoadable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Airloom-Lee-MBP on 12/11/2014.
 */
public class SqlHelper {

    private Connection conn;

    public SqlHelper(Connection conn) {
        this.conn = conn;
    }

    public <T> List<T> query(Class<T> clazz, String sql, Object... params) {

        boolean isResultSetLoadable = ResultSetLoadable.class.isAssignableFrom(clazz);

        try(PreparedStatement stmt = this.createStatement(sql, params)){
            try(ResultSet rs = stmt.executeQuery()){
                return loadListFromResultSet(clazz, isResultSetLoadable, rs);
            }
        }
        catch (Exception ex) {
            throw new RuntimeException("Failed to query: " + sql, ex);
        }
    }

    public ResultSet query(String sql, Object... params) throws SQLException {
        PreparedStatement stmt = this.createStatement(sql, params);
        ResultSet rs = stmt.executeQuery();

        return rs;
    }

    public int update(String sql, Object... params) throws SQLException {
        PreparedStatement stmt = this.createStatement(sql, params);
        return stmt.executeUpdate();
    }

    private PreparedStatement createStatement(String sql, Object... params) throws SQLException {
        PreparedStatement statement = this.conn.prepareStatement(sql);

        if (params != null) {
            int index = 1;
            for (Object param : params) {
                if (param == null) {
                    statement.setNull(index, Types.NULL);
                }
                else{
                    statement.setObject(index, param);
                }

                index ++;
            }
        }

        return  statement;
    }

    private <T> List<T> loadListFromResultSet(Class<T> clazz, boolean isResultSetLoadable, ResultSet rs) throws SQLException, InstantiationException, IllegalAccessException, InvocationTargetException {
        Method[] methods = clazz.getMethods();

        ArrayList<T> list = new ArrayList<T>();

        while (rs.next()){
            T t = (T) clazz.newInstance();

            if (isResultSetLoadable){
                ResultSetLoadable loadable = (ResultSetLoadable) t;
                loadable.loadFromResultSet(rs);
            }
            else {
                initObjFromResultSet(rs, methods, t);
            }

            list.add(t);
        }

        return  list;
    }

    private void initObjFromResultSet(ResultSet rs, Method[] methods, Object obj) throws IllegalAccessException, InvocationTargetException, SQLException {
        for(Method method : methods){
            String methodName = method.getName();
            if (methodName.startsWith("set")){
                String columnName = methodName.substring(3);
                method.invoke(obj, rs.getObject(columnName));
            }
        }
    }

}
