package sqliteMgrJ.helper;

import javafx.concurrent.Task;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.StyleSpans;
import org.fxmisc.richtext.StyleSpansBuilder;
import sqliteMgrJ.model.DatabaseNodeInfo;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Airloom-Lee-MBP on 19/11/2014.
 */
public class SqlStyleHelper {


    private static final String[] PredefinedKeywords = new String[] {
            "SELECT", "FROM", "AS", "WHERE", "NOT", "LIMIT", "ORDER", "BY", "GROUP",
            "MIN", "MAX", "AVG", "COUNT", "CASE", "EXISTS", "WHEN",
            "DELETE", "UPDATE", "PRAGMA",
            "INNER", "LEFT", "OUT", "JOIN", "ON",
            "CREATE", "DROP", "FOREIGN", "KEY",
            "INDEX"
    };

    private final String keywordPattern;
    private static final String PAREN_PATTERN = "\\(|\\)";
    private static final String BRACE_PATTERN = "\\{|\\}";
    private static final String BRACKET_PATTERN = "\\[|\\]";
    private static final String SEMICOLON_PATTERN = "\\;";
//    private static final String STRING_PATTERN = "\"([^\"]|\\\")*\"";

    private static final String STRING_PATTERN = "\"([^\"]|\"\")*\"";
    private static final String SINGLE_QUOTED_STRING_PATTERN = "(?:^|\\s)'([^']*?)'(?:$|\\s)";

    private final Pattern allPatterns;

    private CodeArea codeArea;
    private ExecutorService executor;

    public SqlStyleHelper(CodeArea codeArea, ExecutorService executor, DatabaseNodeInfo dbNodeInfo) {
        this.codeArea = codeArea;
        this.executor = executor;

        this.keywordPattern = "\\b(" + String.join("|", PredefinedKeywords) + ")\\b";
        Set<String> schemaObjs = new HashSet<>();

        dbNodeInfo.getTables().forEach(tbl -> {
            schemaObjs.add(tbl.getName());
            schemaObjs.add("\"" + tbl.getName() + "\"");

            tbl.getColumns().forEach(col -> {
                schemaObjs.add(col.getName());
                schemaObjs.add("\"" + col.getName() + "\"");

                //schemaObjs.add("\"" + tbl.getName() + "\".\"" + col.getName() + "\"");
            });
        });

        String schemaObjsPattern = "\\b(" + String.join("|", schemaObjs) + ")\\b";

        this.allPatterns = Pattern.compile(
                "(?<KEYWORD>" + keywordPattern + ")"
                        + "|(?<SCHEMAOBJS>" + schemaObjsPattern + ")"
                        + "|(?<PAREN>" + PAREN_PATTERN + ")"
                        + "|(?<BRACE>" + BRACE_PATTERN + ")"
                        + "|(?<BRACKET>" + BRACKET_PATTERN + ")"
                        + "|(?<SEMICOLON>" + SEMICOLON_PATTERN + ")"
                        + "|(?<STRING>" + STRING_PATTERN + ")"
                        + "|(?<SINGLEQUOTEDSTRING>" + SINGLE_QUOTED_STRING_PATTERN + ")"
                , Pattern.CASE_INSENSITIVE
        );
    }

    public Task<StyleSpans<Collection<String>>> computeHighlightingAsync() {
        String text = codeArea.getText();
        Task<StyleSpans<Collection<String>>> task = new Task<StyleSpans<Collection<String>>>() {
            @Override
            protected StyleSpans<Collection<String>> call() throws Exception {
                return computeHighlighting(text);
            }
        };
        executor.execute(task);
        return task;
    }

    public void applyHighlighting(StyleSpans<Collection<String>> highlighting) {
        codeArea.setStyleSpans(0, highlighting);
    }

    private StyleSpans<Collection<String>> computeHighlighting(String text) {
        Matcher matcher = allPatterns.matcher(text);
        int lastKwEnd = 0;
        StyleSpansBuilder<Collection<String>> spansBuilder
                = new StyleSpansBuilder<>();
        while(matcher.find()) {
            String styleClass =
                    matcher.group("KEYWORD") != null ? "keyword" :
                            matcher.group("SCHEMAOBJS") != null ? "schema-obj" :
                                matcher.group("PAREN") != null ? "paren" :
                                        matcher.group("BRACE") != null ? "brace" :
                                                matcher.group("BRACKET") != null ? "bracket" :
                                                        matcher.group("SEMICOLON") != null ? "semicolon" :
                                                                matcher.group("STRING") != null ? "string" :
                                                                        matcher.group("SINGLEQUOTEDSTRING") != null ? "single-quoted-string" :
                                                                        null; /* never happens */ assert styleClass != null;
            spansBuilder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
            spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
            lastKwEnd = matcher.end();
        }
        spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);
        return spansBuilder.create();
    }

}
