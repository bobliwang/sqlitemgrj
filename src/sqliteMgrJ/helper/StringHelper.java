package sqliteMgrJ.helper;

import org.fxmisc.richtext.StyledTextArea;
import sqliteMgrJ.model.intellisense.JoinStructure;
import sqliteMgrJ.model.intellisense.JoinedTable;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Airloom-Lee-MBP on 18/11/2014.
 */
public class StringHelper {

    private static final String[] WhiteSpaces = new String[] {" ", "\t", "\r", "\t", "\r\n", "\n"};


    private static final Pattern JoinMatchPattern = Pattern.compile("(?<SELECT>\\b(SELECT)\\b)"
                    + "|(?<FROM>\\b(FROM)\\b)"
                    + "|(?<AS>\\b(AS)\\b)"
                    + "|(?<JOIN>\\b(JOIN)\\b)"
            , Pattern.CASE_INSENSITIVE
    );

    public static boolean isWhiteSpace(String str){
        for(String ws : WhiteSpaces){
            if (ws.equals(str)){
                return true;
            }
        }

        return false;
    }

    public static void selectCurrentWord(StyledTextArea textArea){
        int currentPos = textArea.getCaretPosition();
        int start = findStartOfCurrentWord(textArea, currentPos);

        if (start <= currentPos){
            textArea.selectRange(start, currentPos);
        }
    }

    public static int findStartOfCurrentWord(StyledTextArea textArea, int fromPosition){

        int startPos = fromPosition;
        while (startPos > 0 && !"\"".equalsIgnoreCase(textArea.getText(startPos - 1, startPos)) && !StringHelper.isWhiteSpace(textArea.getText(startPos - 1, startPos))){
            startPos--;
        }
        return startPos;
    }

    public static String getCurrentWord(StyledTextArea textArea, int fromPosition){

        int startPos = findStartOfCurrentWord(textArea, fromPosition);

        return textArea.getText(startPos, textArea.getCaretPosition());
    }

    public static String getCurrentWord(StyledTextArea textArea){

        int startPos = findStartOfCurrentWord(textArea, textArea.getCaretPosition());

        return textArea.getText(startPos, textArea.getCaretPosition());
    }

    public static void replaceCurrentWord(StyledTextArea textArea, int fromPosition, String text){

        int startPos = findStartOfCurrentWord(textArea, fromPosition);

        textArea.replaceText(startPos, fromPosition, text);
    }

    public static void replaceCurrentWord(StyledTextArea textArea, String text){
        replaceCurrentWord(textArea, textArea.getCaretPosition(), text);
    }

    public static String getStackTrace(Throwable throwable){
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);

        throwable.printStackTrace(pw);

        return sw.getBuffer().toString();
    }


    public static String toRowVersion(byte[] rowVersion){
        if (rowVersion != null && rowVersion.length == 8)
        {
            return "0x" + javax.xml.bind.DatatypeConverter.printHexBinary(rowVersion);
        }

        if (rowVersion == null)
        {
            return "NULL";
        }

        return "Unable to render binary data";
    }

    public static JoinStructure findJoinStructure(String text){
        Matcher matcher = JoinMatchPattern.matcher(text);
        JoinStructure joinStructure = new JoinStructure();

        while(matcher.find()){
            if (matcher.group("AS") != null){
                String nextWord = getNextWord(text, matcher.end());

                JoinedTable lastTable = joinStructure.getLastTable();
                if (lastTable != null){
                    lastTable.setAlias(nextWord);
                }
            }

            if (matcher.group("FROM") != null){
                joinStructure.setJoinKeywordFound(false); // reset - join not found yet
                joinStructure.getTables().clear();
                JoinedTable table = new JoinedTable(getNextWord(text, matcher.end()));
                joinStructure.addTable(table);
            }

            if (matcher.group("JOIN") != null){
                joinStructure.setJoinKeywordFound(true);
                JoinedTable table = new JoinedTable(getNextWord(text, matcher.end()));
                joinStructure.addTable(table);
            }
        }

        return joinStructure.isJoinKeywordFound() ? joinStructure : null;
    }

    public static JoinStructure findJoinStructure(StyledTextArea textArea, int beforeCaretPosition){
        String text = textArea.getText(0, beforeCaretPosition);
        return  findJoinStructure(text);
    }

    public static JoinStructure findJoinStructure(StyledTextArea textArea) {
        int beforeCaretPosition = textArea.getCaretPosition();
        return findJoinStructure(textArea, beforeCaretPosition);
    }

    private static String getNextWord(String str, int startPos){
        int i = startPos;

        while(i < str.length() && isWhiteSpace(str.substring(i, i+1))){
            i++;
        }

        startPos = i;

        while(i < str.length() && !isWhiteSpace(str.substring(i, i+1))){
            i++;
        }

        if (startPos < str.length()) {
            return str.substring(startPos, i);
        }

        return "";
    }

    public static String findTable2InJoin(StyledTextArea textArea) {
        return "AccountNotes";
    }
}
