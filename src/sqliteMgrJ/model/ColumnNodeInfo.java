package sqliteMgrJ.model;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Airloom-Lee-MBP on 12/11/2014.
 */
public class ColumnNodeInfo extends  NodeInfo implements ResultSetLoadable {


    private String type;

    private long pk;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getPk() {
        return pk;
    }

    public void setPk(long pk) {
        this.pk = pk;
    }

    @Override
    public void loadFromResultSet(ResultSet rs) throws SQLException {
        this.setName(rs.getString("Name"));
        this.setType(rs.getString("Type"));
        this.setPk(rs.getLong("Pk"));
    }
}
