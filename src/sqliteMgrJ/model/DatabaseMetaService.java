package sqliteMgrJ.model;

import sqliteMgrJ.helper.SqlHelper;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Airloom-Lee-MBP on 20/12/2014.
 */
public class DatabaseMetaService {
    private SqlHelper sqlHelper;

    public DatabaseMetaService(SqlHelper sqlHelper) {
        this.sqlHelper = sqlHelper;
    }

    public void reloadMetadata(DatabaseNodeInfo dbNodeData){
        dbNodeData.getTables().clear();

        List<SqliteMaster> sqliteMasters = sqlHelper.query(SqliteMaster.class, "select Tbl_Name as TableName, Sql, Name, Type from sqlite_master ORDER BY Tbl_Name");


        List<TriggerNodeInfo> triggers = sqliteMasters.stream()
                .filter(x -> "trigger".equalsIgnoreCase(x.getType()))
                .map(x -> x.mapToTriggerNodeInfo())
                .collect(Collectors.toList());

        List<IndexNodeInfo> indexes = sqliteMasters.stream()
                .filter(x -> "index".equalsIgnoreCase(x.getType()))
                .map(x -> x.mapToIndexNodeInfo())
                .collect(Collectors.toList());

        List<TableNodeInfo> tableNodeInfos = sqliteMasters.stream()
                .filter(x -> "table".equalsIgnoreCase(x.getType())
                        || "view".equalsIgnoreCase(x.getType()))
                .map(x -> x.mapToTableNodeInfo())
                .collect(Collectors.toList());

        for(TableNodeInfo tableNodeInfo : tableNodeInfos){
            for (TriggerNodeInfo trigger : triggers){
                if (trigger.getTable().equalsIgnoreCase(tableNodeInfo.getName())){
                    tableNodeInfo.getTriggers().add(trigger);
                }
            }

            for (IndexNodeInfo index : indexes){
                if (index.getTable().equalsIgnoreCase(tableNodeInfo.getName())){
                    tableNodeInfo.getIndexes().add(index);
                }
            }

            List<ColumnNodeInfo> columnNodeInfos = sqlHelper.query(ColumnNodeInfo.class, "PRAGMA table_info('" + tableNodeInfo.getName() + "')");
            tableNodeInfo.getColumns().addAll(columnNodeInfos);
        }


        dbNodeData.getTables().addAll(tableNodeInfos);
    }
}
