package sqliteMgrJ.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Airloom-Lee-MBP on 12/11/2014.
 */
public class DatabaseNodeInfo extends NodeInfo {

    private String filePath;

    public String getConnectionString() {
        return connectionString;
    }

    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    private String connectionString = "";

    private List<TableNodeInfo> tables = new ArrayList<TableNodeInfo>();

    public List<TableNodeInfo> getTables() {
        return tables;
    }

    public void setTables(List<TableNodeInfo> tables) {
        this.tables = tables;
    }

    public DatabaseNodeInfo(String filePath) {
        this.setFilePath(filePath);
    }

    public DatabaseNodeInfo(String name, String filePath) {
        super(name);
        this.setFilePath(filePath);
    }

    public DatabaseNodeInfo(String name, String displayName, String filePath) {
        super(name, displayName);
        this.setFilePath(filePath);
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }


    @Override
    public String getDescription() {
        return this.getName() + "\r\n" + this.getFilePath() + "\r\n" + this.getConnectionString();
    }

    public TableNodeInfo findTableNodeInfo(String name){
        if (name.startsWith("\"") && name.endsWith("\"")){
            name = name.replace("\"", "");
        }

        final String key = name;

        Optional<TableNodeInfo> result = this.getTables().stream().filter(x -> key.equalsIgnoreCase(x.getName())).findFirst();

        return result.isPresent() ? result.get() : null;
    }


}
