package sqliteMgrJ.model;

/**
 * Created by Airloom-Lee-MBP on 12/11/2014.
 */
public class NodeInfo {

    private String name;

    private String displayName;

    public NodeInfo() {
        this("");
    }

    public NodeInfo(String name) {
        this(name, name);
    }

    public NodeInfo(String name, String displayName){
        this.setName(name);
        this.setDisplayName(displayName);
    }


    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }


    @Override
    public String toString() {
        return this.getDisplayName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return  this.getDisplayName();
    }
}
