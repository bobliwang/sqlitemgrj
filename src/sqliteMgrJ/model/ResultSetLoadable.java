package sqliteMgrJ.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Airloom-Lee-MBP on 13/11/2014.
 */
public interface ResultSetLoadable {

    void loadFromResultSet(ResultSet rs) throws SQLException;
}
