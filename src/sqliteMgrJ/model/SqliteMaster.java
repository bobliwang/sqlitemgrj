package sqliteMgrJ.model;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Airloom-Lee-MBP on 18/11/2014.
 */
public class SqliteMaster implements ResultSetLoadable {

    private String name;

    private String sql;

    private String table;

    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void loadFromResultSet(ResultSet rs) throws SQLException {
        this.setName(rs.getString("Name"));
        this.setTable(rs.getString("TableName"));
        this.setType(rs.getString("Type"));
        this.setSql(rs.getString("Sql"));
    }

    public TriggerNodeInfo mapToTriggerNodeInfo(){
        TriggerNodeInfo triggerNodeInfo = new TriggerNodeInfo();

        triggerNodeInfo.setName(this.getName());
        triggerNodeInfo.setTable(this.getTable());
        triggerNodeInfo.setSql(this.getSql());
        triggerNodeInfo.setDisplayName(this.getName());

        return triggerNodeInfo;
    }

    public TableNodeInfo mapToTableNodeInfo() {
        TableNodeInfo tableNodeInfo = new TableNodeInfo();

        tableNodeInfo.setName(this.getName());
        tableNodeInfo.setSql(this.getSql());
        tableNodeInfo.setDisplayName(this.getName());
        tableNodeInfo.setType(this.getType());

        return tableNodeInfo;

    }

    public IndexNodeInfo mapToIndexNodeInfo() {
        IndexNodeInfo nodeInfo = new IndexNodeInfo();

        nodeInfo.setName(this.getName());
        nodeInfo.setTable(this.getTable());
        nodeInfo.setSql(this.getSql());
        nodeInfo.setDisplayName(this.getName());

        return nodeInfo;
    }
}
