package sqliteMgrJ.model;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Airloom-Lee-MBP on 12/11/2014.
 */

public class TableNodeInfo extends NodeInfo implements ResultSetLoadable {

    private String sql;

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private List<ColumnNodeInfo> columns = new ArrayList<>();

    private List<IndexNodeInfo> indexes = new ArrayList<>();

    private List<TriggerNodeInfo> triggers = new ArrayList<>();

    public List<ColumnNodeInfo> getColumns() {
        return columns;
    }

    public void setColumns(List<ColumnNodeInfo> columns) {
        this.columns = columns;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public List<IndexNodeInfo> getIndexes() {
        return indexes;
    }

    public void setIndexes(List<IndexNodeInfo> indexes) {
        this.indexes = indexes;
    }

    public List<TriggerNodeInfo> getTriggers() {
        return triggers;
    }

    public void setTriggers(List<TriggerNodeInfo> triggers) {
        this.triggers = triggers;
    }

    public void loadFromResultSet(ResultSet rs) throws SQLException {
        this.setName(rs.getString("TableName"));
        this.setDisplayName(this.getName());
        this.setSql(rs.getString("Sql"));
        this.setType(rs.getString("Type"));
    }

    public boolean isTable(){
        return "table".equalsIgnoreCase(this.getType());
    }

    @Override
    public String getDescription() {
        return this.getSql();
    }
}
