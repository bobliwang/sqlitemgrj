package sqliteMgrJ.model;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Airloom-Lee-MBP on 17/11/2014.
 */
public class TriggerNodeInfo extends NodeInfo implements ResultSetLoadable {
    private String sql;

    private String table;


    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    @Override
    public void loadFromResultSet(ResultSet rs) throws SQLException {
        this.setName(rs.getString("Name"));
        this.setDisplayName(this.getName());
        this.setSql(rs.getString("Sql"));
        this.setTable(rs.getString("TableName"));
    }


    @Override
    public String getDescription() {
        return this.getSql();
    }
}
