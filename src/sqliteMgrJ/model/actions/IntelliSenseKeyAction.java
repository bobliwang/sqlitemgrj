package sqliteMgrJ.model.actions;

import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Popup;
import org.fxmisc.richtext.NavigationActions;
import org.fxmisc.richtext.PopupAlignment;
import org.fxmisc.richtext.StyledTextArea;
import sqliteMgrJ.helper.StringHelper;
import sqliteMgrJ.model.ColumnNodeInfo;
import sqliteMgrJ.model.DatabaseNodeInfo;
import sqliteMgrJ.model.TableNodeInfo;

import javafx.scene.input.*;
import sqliteMgrJ.model.intellisense.IntelliSenseItem;
import sqliteMgrJ.model.intellisense.IntelliSenseItemType;
import sqliteMgrJ.model.intellisense.JoinStructure;
import sqliteMgrJ.model.intellisense.JoinedTable;
import sqliteMgrJ.view.IntellisenseItemCell;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Airloom-Lee-MBP on 13/11/2014.
 */
public class IntelliSenseKeyAction extends KeyAction {

    private DatabaseNodeInfo databaseNodeInfo;

    public IntelliSenseKeyAction(KeyCode keyCode, DatabaseNodeInfo databaseNodeInfo) {
        super(keyCode);
        this.databaseNodeInfo = databaseNodeInfo;
    }

    @Override
    public void perform(KeyEvent keyEvent, StyledTextArea textArea) {
        Popup popup = this.getPopupFrom(textArea);
        StringHelper.selectCurrentWord(textArea);
        ListView<IntelliSenseItem> lv = (ListView) popup.getContent().get(0);
        this.setupItemsForPopup(lv, popup, textArea);

        textArea.getPopupWindow().show(textArea.getScene().getWindow());
    }

    private void setupItemsForPopup(ListView listView, Popup popup, StyledTextArea textArea) {
        listView.getItems().clear();

        String currentWord = textArea.getSelectedText();
        if(currentWord == null || currentWord.isEmpty()){
            currentWord = StringHelper.getCurrentWord(textArea);
        }

        if ("{table}".equalsIgnoreCase(currentWord)){
            this.loadTableItems(listView);
        }
        else if ("{column}".equalsIgnoreCase(currentWord)){
            this.loadColumnItems(listView);
        }
        else {
            if ("on".equalsIgnoreCase(currentWord)){
                this.loadJoinConditions(listView, textArea);
            }

            this.loadTableItems(listView, currentWord);
            this.loadColumnItems(listView, currentWord);
        }

        this.sortItems(listView.getItems(), currentWord);
        if (!listView.getItems().isEmpty()){
            listView.scrollTo(0);
            listView.getSelectionModel().select(0);
        }
    }

    private void loadTableItems(ListView listView, String keyword){
        for(TableNodeInfo tb : databaseNodeInfo.getTables()){
            if (keyword == null || tb.getName().toLowerCase().contains(keyword.toLowerCase())){
                listView.getItems().add(new IntelliSenseItem(tb.getName(), tb.isTable() ? IntelliSenseItemType.Table : IntelliSenseItemType.View));
            }
        }
    }

    private void loadTableItems(ListView listView){
        this.loadTableItems(listView, null);
    }

    private void loadColumnItems(ListView listView, String keyword){

        Set<IntelliSenseItem> set = new HashSet<>();

        for(TableNodeInfo tb : databaseNodeInfo.getTables()){
            for(ColumnNodeInfo col : tb.getColumns()){
                String value = tb.getName() + "." + col.getName();
                if (keyword == null || value.toLowerCase().contains(keyword.toLowerCase())){
                    set.add(new IntelliSenseItem(value, IntelliSenseItemType.Column));
                }

                value = col.getName();
                if (keyword == null || value.toLowerCase().contains(keyword.toLowerCase())){
                    set.add(new IntelliSenseItem(value, IntelliSenseItemType.Column));
                }
            }
        }

        listView.getItems().addAll(set);
    }

    private void loadJoinConditions(ListView listView, StyledTextArea textArea){

        JoinStructure joinStructure = StringHelper.findJoinStructure(textArea);

        if (joinStructure == null || !joinStructure.isJoinKeywordFound()){
            return;
        }

        System.out.println(joinStructure.toString());

        JoinedTable lastJoinedTable = joinStructure.getLastTable();
        TableNodeInfo lastJoinTableNodeInfo = databaseNodeInfo.findTableNodeInfo(lastJoinedTable.getTableName());

        for(int i = joinStructure.getTables().size() - 2; i >= 0; i--){
            JoinedTable jt = joinStructure.getTables().get(i);

            TableNodeInfo anotherTableNodeInfo = databaseNodeInfo.findTableNodeInfo(jt.getTableName());

            if (anotherTableNodeInfo != null){
                lastJoinTableNodeInfo.getColumns().forEach(col1 -> {
                    anotherTableNodeInfo.getColumns().forEach(col2 -> {
                        if (col1.getType().equals(col2.getType())
                                && (col1.getName().contains(col2.getName()) || col2.getName().contains(col1.getName()))){
                            String text = lastJoinedTable.getNameOnClause() + "." + col1.getName() + " = " + jt.getNameOnClause() + "." + col2.getName();
                            listView.getItems().add(new IntelliSenseItem(text, IntelliSenseItemType.JoinCondition));
                        }
                    });
                });
            }

        }
    }

    private void loadColumnItems(ListView listView){
        this.loadColumnItems(listView, null);
    }

    private void sortItems(ObservableList<IntelliSenseItem> items, String keyword){
        if (keyword != null && !keyword.isEmpty()){
            items.sort((item1, item2) -> {
                String str1 = item1.getText();
                String str2 = item2.getText();

                if (item1.isJoinCondition() && !item2.isJoinCondition()){
                    return -1;
                }
                if (!item1.isJoinCondition() && item2.isJoinCondition()){
                    return 1;
                }

                if (keyword.equals(str1)){
                    return -1;
                }

                if (keyword.equals(item2)){
                    return 1;
                }

                if (keyword.equalsIgnoreCase(str1)){
                    return -1;
                }

                if (keyword.equalsIgnoreCase(str2)){
                    return 1;
                }

                if (str1.startsWith(keyword)
                        && !str2.startsWith(keyword)){
                    return -1;
                }
                if (str2.startsWith(keyword)
                        && !str1.startsWith(keyword)){
                    return 1;
                }

                if (str1.toLowerCase().startsWith(keyword.toLowerCase())
                        && !str2.toLowerCase().startsWith(keyword.toLowerCase())){
                    return -1;
                }

                if (str2.toLowerCase().startsWith(keyword.toLowerCase())
                        && !str1.toLowerCase().startsWith(keyword.toLowerCase())){
                    return 1;
                }

                if (keyword.length() > 2 && str1.toLowerCase().startsWith(keyword.toLowerCase())
                        && str2.toLowerCase().startsWith(keyword.toLowerCase())){

                    if (item1.isTableOrView() && !item2.isTableOrView()){
                        return -1;
                    }

                    if (item2.isTableOrView() && !item1.isTableOrView()){
                        return 1;
                    }

                    return str1.length() - str2.length();
                }

                if (str1.contains(keyword)
                        && !str2.contains(keyword)){
                    return -1;
                }

                if (str2.contains(keyword)
                        && !str1.contains(keyword)){
                    return 1;
                }

                if (item1.isTableOrView() && !item2.isTableOrView()){
                    return -1;
                }

                if (item2.isTableOrView() && !item1.isTableOrView()){
                    return 1;
                }

                return str1.compareTo(str2);
            });
        }
        else {
            items.sort((item1, item2) -> {
                String str1 = item1.getText();
                String str2 = item2.getText();

                if (item1.isTableOrView() && !item2.isTableOrView()){
                    return -1;
                }

                if (item2.isTableOrView() && !item1.isTableOrView()){
                    return 1;
                }

                return str1.compareTo(str2);
            });
        }
    }

    private Popup getPopupFrom(StyledTextArea textArea){

        if (textArea.getPopupWindow() == null){
            this.setupPopupForTextArea(textArea);
        }

        return (Popup)textArea.getPopupWindow();
    }

    private void setupPopupForTextArea(StyledTextArea textArea) {
        final Popup popup = new Popup();
        textArea.setPopupWindow(popup);
        popup.setAutoHide(true);
        popup.setHideOnEscape(true);

        final ListView<IntelliSenseItem> lv = new ListView();
        popup.getContent().add(lv);
        lv.setCellFactory(view -> {
            return new IntellisenseItemCell();
        });

        lv.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER || (event.getCode() == KeyCode.SPACE && !event.isControlDown())) {

                this.insertTextFromListViewToTextArea(textArea, lv);

                popup.hide();
                event.consume();
            }
            else if (event.getCode() == KeyCode.ESCAPE) {
                popup.hide();
                event.consume();
            }
        });

        lv.setOnMouseClicked( event -> {
            if (event.getButton() == MouseButton.PRIMARY) {

                this.insertTextFromListViewToTextArea(textArea, lv);

                popup.hide();
                event.consume();
            }
        });

        textArea.textProperty().addListener((observableValue, oldText, newText) -> {
            if (popup.isShowing()){
                this.setupItemsForPopup(lv, popup, textArea);
            }
        });

        textArea.setPopupAlignment(PopupAlignment.CARET_CENTER);
        textArea.setPopupAnchorOffset(new Point2D(4, 0));
    }

    private void insertTextFromListViewToTextArea(StyledTextArea textArea, ListView lv) {
        IntelliSenseItem selectedItem = (IntelliSenseItem)lv.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            String textToInsert = selectedItem.getTextToInsert();

            String currentWord = textArea.getSelectedText();
            if(currentWord == null || currentWord.isEmpty()){
                StringHelper.replaceCurrentWord(textArea, textToInsert);
            }
            else {
                textArea.replaceSelection(textToInsert);
            }
        }
    }

}
