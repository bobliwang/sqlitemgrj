package sqliteMgrJ.model.actions;

import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.fxmisc.richtext.StyledTextArea;
import sqliteMgrJ.model.shortcuts.SqlShortCut;


import java.util.*;
import java.util.List;

/**
 * Created by Airloom-Lee-MBP on 13/11/2014.
 */
public class KeyAction {

    private KeyCode keyCode;

    public KeyAction(KeyCode keyCode) {
        this.keyCode = keyCode;
    }

    public KeyCode getKeyCode() {
        return keyCode;
    }

    public void setKeyCode(KeyCode keyCode) {
        this.keyCode = keyCode;
    }

    public void perform(KeyEvent keyEvent, StyledTextArea textArea) {

    }

    public static List<KeyAction> PredefinedKeyActions = Arrays.asList(
            new SqlShortCutKeyAction(KeyCode.TAB, SqlShortCut.getPredefinedShortCuts())
    );
}
