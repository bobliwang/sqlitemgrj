package sqliteMgrJ.model.actions;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.fxmisc.richtext.NavigationActions;
import org.fxmisc.richtext.StyledTextArea;
import sqliteMgrJ.helper.StringHelper;
import sqliteMgrJ.model.shortcuts.SqlShortCut;

import javafx.scene.control.TextArea;

import java.util.List;

/**
 * Created by Airloom-Lee-MBP on 13/11/2014.
 */
public class SqlShortCutKeyAction extends KeyAction {

    private List<SqlShortCut> shortCuts;

    public SqlShortCutKeyAction(KeyCode keyCode, List<SqlShortCut> shortCuts) {
        super(keyCode);
        this.shortCuts = shortCuts;
    }

    @Override
    public void perform(KeyEvent keyEvent, StyledTextArea txtSql) {
        int currentPos = txtSql.getCaretPosition();
        int startPos = StringHelper.findStartOfCurrentWord(txtSql, currentPos);

        // not including the part after the caret
        final String currentWord = txtSql.getText(startPos, currentPos);
        for(SqlShortCut shortcut : this.shortCuts){
            if (shortcut.getShortCutKeys().equalsIgnoreCase(currentWord.trim())){
                txtSql.replaceText(startPos, currentPos, shortcut.getFullString());

                keyEvent.consume();
                return;
            }
        }
    }
}
