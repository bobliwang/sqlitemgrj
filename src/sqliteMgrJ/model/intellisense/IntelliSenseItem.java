package sqliteMgrJ.model.intellisense;

/**
 * Created by Airloom-Lee-MBP on 20/11/2014.
 */
public class IntelliSenseItem {

    private String text;

    private IntelliSenseItemType type;

    public IntelliSenseItem() {
    }

    public IntelliSenseItem(String labelText, IntelliSenseItemType type) {
        this.text = labelText;
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public String getTextToInsert(){
        if (this.getType() != IntelliSenseItemType.JoinCondition) {
            return this.getText();
        }

        return "ON " + this.getText();
    }

    public void setText(String text) {
        this.text = text;
    }

    public IntelliSenseItemType getType() {
        return type;
    }

    public void setType(IntelliSenseItemType type) {
        this.type = type;
    }

    public String getLabelText(){
        return this.getText();
    }

    public boolean isTableOrView(){
        return this.getType() == IntelliSenseItemType.Table || this.getType() == IntelliSenseItemType.View;
    }

    public boolean isJoinCondition(){
        return this.getType() == IntelliSenseItemType.JoinCondition;
    }

    @Override
    public String toString() {
        return getLabelText();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof IntelliSenseItem)){
            return  false;
        }

        IntelliSenseItem another = (IntelliSenseItem) obj;

        return another.getType() == this.getType() && another.getLabelText().equals(this.getLabelText());
    }

    @Override
    public int hashCode() {
        return this.getLabelText().hashCode() + this.getType().hashCode();
    }
}
