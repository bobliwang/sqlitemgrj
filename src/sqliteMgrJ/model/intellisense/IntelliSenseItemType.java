package sqliteMgrJ.model.intellisense;

/**
 * Created by Airloom-Lee-MBP on 20/11/2014.
 */
public enum IntelliSenseItemType {
    Table,
    View,
    Column,

    JoinCondition,
}
