package sqliteMgrJ.model.intellisense;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Airloom-Lee-MBP on 20/11/2014.
 */
public class JoinStructure {

    private List<JoinedTable> tables;

    private boolean joinKeywordFound;

    public JoinStructure() {
        this.tables = new ArrayList<>();
    }

    public void addTable(JoinedTable joinedTable){
        this.getTables().add(joinedTable);
    }

    public boolean isJoinKeywordFound() {
        return joinKeywordFound;
    }

    public void setJoinKeywordFound(boolean joinKeywordFound) {
        this.joinKeywordFound = joinKeywordFound;
    }

    public List<JoinedTable> getTables() {
        return tables;
    }

    public JoinedTable getLastTable(){
        if (!this.getTables().isEmpty()){
            return this.getTables().get(this.getTables().size() - 1);
        }

        return null;
    }

    public JoinedTable getFirstTable(){
        if (!this.getTables().isEmpty()){
            return this.getTables().get(0);
        }

        return null;
    }

    public String getFromTable() {
        JoinedTable firstTable = this.getFirstTable();
        return firstTable != null ? firstTable.getTableName() : null;
    }

    public String getFromAlias() {
        JoinedTable firstTable = this.getFirstTable();
        return firstTable != null ? firstTable.getAlias() : null;
    }

    public String getLastJoinTable() {
        JoinedTable lastTable = this.getLastTable();
        return lastTable != null ? lastTable.getTableName() : null;
    }

    public String getLastJoinAlias() {
        JoinedTable lastTable = this.getLastTable();
        return lastTable != null ? lastTable.getAlias() : null;
    }

    public String getFrom(){
        return isNullOrEmpty(this.getFromAlias()) ? this.getFromTable() : this.getFromAlias();
    }

    public String getLastJoin(){
        return isNullOrEmpty(this.getLastJoinAlias()) ? this.getLastJoinTable() : this.getLastJoinAlias();
    }

    private boolean isNullOrEmpty(String str){
        return str == null || str.isEmpty();
    }



    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append("From Table:" + this.getFromTable());
        sb.append("\r\n");
        sb.append("From Alias:" + this.getFromAlias());
        sb.append("\r\n");
        sb.append("Join Table:" + this.getLastJoinTable());
        sb.append("\r\n");
        sb.append("Join Alias:" + this.getLastJoinAlias());
        sb.append("\r\n");


        return sb.toString();
    }
}
