package sqliteMgrJ.model.intellisense;

/**
 * Created by Airloom-Lee-MBP on 21/11/2014.
 */
public class JoinedTable {

    private String tableName;

    private String alias;

    public JoinedTable(String tableName) {
        this.tableName = tableName;
        this.alias = tableName;
    }

    public JoinedTable(String tableName, String alias) {
        this.tableName = tableName;
        this.alias = alias;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getNameOnClause(){
        return isNullOrWhitespaces(this.getAlias()) ? this.getTableName() : this.getAlias();
    }

    private boolean isNullOrWhitespaces(String str){
        return str == null || "".equals(str.trim());
    }
}
