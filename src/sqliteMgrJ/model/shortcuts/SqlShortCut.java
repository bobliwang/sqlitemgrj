package sqliteMgrJ.model.shortcuts;

import java.util.*;

/**
 * Created by Airloom-Lee-MBP on 13/11/2014.
 */
public class SqlShortCut {

    private String shortCutKeys;

    private String fullString;

    public SqlShortCut(String shortCutKeys, String fullString) {
        this.shortCutKeys = shortCutKeys;
        this.fullString = fullString;
    }

    public String getShortCutKeys() {
        return shortCutKeys;
    }

    public void setShortCutKeys(String shortCutKeys) {
        this.shortCutKeys = shortCutKeys;
    }

    public String getFullString() {
        return fullString;
    }

    public void setFullString(String fullString) {
        this.fullString = fullString;
    }


    private static List<SqlShortCut> PredefinedShortCuts = Arrays.asList(
            new SqlShortCut("SSF", "SELECT * FROM {Table}"),
            new SqlShortCut("SCF", "SELECT COUNT(*) FROM {Table}"),
            new SqlShortCut("DF", "DELETE FROM {Table}"),
            new SqlShortCut("INS", "INSERT INTO {Table} VALUES ({Values})"),
            new SqlShortCut("WH", "WHERE "),
            new SqlShortCut("OB", "ORDER BY {Column}"),
            new SqlShortCut("GB", "GROUP BY {Column}"),
            new SqlShortCut("PRA", "PRAGMA table_info('{Table}')"),
            new SqlShortCut("IJ", "INNER JOIN {Table}"),
            new SqlShortCut("LJ", "LEFT JOIN {Table}"),
            new SqlShortCut("IJO", "INNER JOIN {Table} ON "),
            new SqlShortCut("LJO", "LEFT JOIN {Table} ON "),
            new SqlShortCut("LIM", "LIMIT ")
    );

    public static List<SqlShortCut> getPredefinedShortCuts(){
        return PredefinedShortCuts;
    }
}
