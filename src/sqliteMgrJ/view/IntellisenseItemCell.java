package sqliteMgrJ.view;

import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import sqliteMgrJ.helper.ImageHelper;
import sqliteMgrJ.model.intellisense.IntelliSenseItem;

/**
 * Created by Airloom-Lee-MBP on 21/11/2014.
 */
public class IntellisenseItemCell extends ListCell<IntelliSenseItem> {

    @Override
    public void updateItem(IntelliSenseItem item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {

            this.setText(item.toString());

            ImageView imageView = null;
            switch (item.getType()){
                case Column:
                    imageView = ImageHelper.FromFile("Column.png", 16);
                    break;
                case Table:
                    imageView = ImageHelper.FromFile("Table_32.png", 16);
                    break;
                case View:
                    imageView = ImageHelper.FromFile("dataView.png", 16);
                    break;
                case JoinCondition:
                    imageView = ImageHelper.FromFile("ForeignKey.png", 16);
                    break;
            }

            setGraphic(imageView);
        }
    }
}
